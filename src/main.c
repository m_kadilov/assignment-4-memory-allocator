#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>

static void test_memory_allocation() {
    void* heap = heap_init(0);
    printf("Check memory allocation:\n");

    debug_heap(stdout, heap);
    void* mem = _malloc(0);
    assert(mem != NULL && "ERROR: memory allocation failed!");

    debug_heap(stdout, heap);
    printf("SUCCESS: memory allocated at %p\n", mem);

    _free(mem);
    heap_term();
    printf("SUCCESS: memory released/\n\n\n");
}

static void test_one_block_release() {
    void* heap = heap_init(0);
    printf("Check release of one memory block:\n");

    debug_heap(stdout, heap);
    void* mem1 = _malloc(64);
    assert(mem1 != NULL && "ERROR: one block allocation failed!");
    
    debug_heap(stdout, heap);
    _free(mem1);
    heap_term();
    printf("SUCCESS: single block released\n\n\n");
}

static void test_multiple_blocks_release() {
    void* heap = heap_init(0);
    printf("Test release of multiple memory blocks\n");

    debug_heap(stdout, heap);
    void* mem1 = _malloc(64);
    void* mem2 = _malloc(128);
    assert(mem1 != NULL && "ERROR: first block allocation failed!");
    assert(mem2 != NULL && "ERROR: second block allocation failed!");

    debug_heap(stdout, heap);
    _free(mem1);
    _free(mem2);
    heap_term();
    printf("SUCCESS: multiple blocks released\n\n\n");
}

static void test_heap_extension() {
    void* heap = heap_init(0);
    printf("Test heap extension\n");

    debug_heap(stdout, heap);
    void* mem1 = _malloc(4096);
    void* mem2 = _malloc(8129);
    assert(mem1 != NULL && "ERROR: failed in initial allocation!");
    assert(mem2 != NULL && "ERROR: failed in extended allocation!");

    debug_heap(stdout, heap);
    _free(mem1);
    _free(mem2);
    heap_term();
    printf("SUCCESS: the heap expansion and deallocation function is correct\n\n");
}

static void test_heap_extension_to_another_place() {
    void* heap = heap_init(0);
    printf("Test heap extension with an obstacle\n");

    debug_heap(stdout, heap);
    void* obstacle = _malloc(4096);
    void* mem = _malloc(8192);
    assert(obstacle != NULL && "ERROR: failed to allocate obstacle block!");
    assert(mem != NULL && "ERROR: failed to allocate with obstacle in place!");

    debug_heap(stdout, heap);
    _free(obstacle);
    _free(mem);
    heap_term();
    printf("SUCCESS: managed heap extension with obstacle and freed memory\n\n");
}

int main() {
    printf("Test 1\n");
    test_memory_allocation();

    printf("Test 2\n");
    test_one_block_release();

    printf("Test 3\n");
    test_multiple_blocks_release();

    printf("Test 4\n");
    test_heap_extension();

    printf("Test 5\n");
    test_heap_extension_to_another_place();

    return 0;
}
